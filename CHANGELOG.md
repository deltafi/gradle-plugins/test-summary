# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

All [Unreleased] changes can be viewed in GitLab.

## [1.0.0] - 2023-07-20

### Upgrade and Migration
- Updated plugin release location


[Unreleased]: https://gitlab.com/deltafi/deltafi/-/compare/1.0.0...main
[1.0.0]: https://gitlab.com/deltafi/deltafi/-/compare/0.0.1...1.0.0
